# Changelog

## Unreleased

## 3.0.0 - 2025-02-27

### Changed

- Compatible SPIP 4.2 minimum
- Chaînes de langue au format SPIP 4.1+

## 2.1.0 - 2024-07-05

### Changed

- Compatible SPIP 4.*

## 2.0.6 - 2023-06-14

### Changed

- Version compatible SPIP 4.2
