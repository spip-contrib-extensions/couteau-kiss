<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-ck?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'ck_description' => 'Simplicité, efficacité, légèreté.
_ Un couteau qui tient vraiment dans la poche en
une unique page de configuration pour les réglages cachés de SPIP.',
	'ck_nom' => 'KISS Knife',
	'ck_slogan' => 'Simplifier certains réglages SPIP',
];
