<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/174?lang_cible=oc_ni_mis
// ** ne pas modifier le fichier **

return [

	// E
	'erreur_cache_taille_mini' => 'L’amagadou noun pòu estre d’una talha inferiour a 10Mo',
	'erreur_dossier_squelette_invalide' => 'Lou doussié esquèletrou noun pòu estre un camin assoulut ni mancou countenì de referença <tt>../</tt>',
	'explication_dossier_squelettes' => 'Poudès endicà mai d’un repertori separat da ’ :’, que seràn pihat en l’ordre. Lou repertori noumat "<tt>squelettes</tt>" es toujou pihat en darrié s’esista.',
	'explication_image_seuil_document' => 'Li image telecargadi pouòdon estre passadi automaticamen en mòdou doucumen en delà d’una larguessa predefinida',
	'explication_introduction_suite' => 'Lu pounch de coutinuacioun soun ajustat da la balisa <tt>#INTRODUCTION</tt> coura coupa un tèstou. En mancança <tt> (...)</tt>',

	// L
	'label_cache_duree' => 'Durada de l’amagadou',
	'label_cache_duree_recherche' => 'Durada de l’amagadou de la recerca',
	'label_cache_strategie' => 'Estrategìa de l’amagadou',
	'label_cache_strategie_jamais' => 'Mìnga d’amagadou (aquela oupcioun s’anulerà au bout de 24h)',
	'label_cache_strategie_normale' => 'Amagadou a durada limitada',
	'label_cache_strategie_permanent' => 'Amagadou a durada ilimitada',
	'label_cache_taille' => 'Talha de l’amagadour (Mo)',
	'label_compacte_head_ecrire' => 'Toujou coumprimà CSS e javascript',
	'label_derniere_modif_invalide' => 'Metre a jou l’amagadou a cada nouvela publicacioun',
	'label_docs_seuils' => 'Limità la talha dei doucumen dóu tems dóu telecargamen',
	'label_dossier_squelettes' => 'Doussié <tt>squelettes</tt>',
	'label_forcer_lang' => 'Fourçà la lenga de l’url o dóu visitaire (<tt>$forcer_lang</tt>)',
	'label_image_seuil_document' => 'Larguessa dei image mòdou doucumen',
	'label_imgs_seuils' => 'Limità la talha dei image dóu tems dóu telecargamen',
	'label_inhiber_javascript_ecrire' => 'Desativà lou javascript en lu article',
	'label_introduction_suite' => 'Pounch de coutinuacioun',
	'label_logo_seuils' => 'Limità la talha dei lògou dóu tems dóu telecargamen',
	'label_longueur_login_mini' => 'Lounguessa minimala dei login',
	'label_max_height' => 'Autessa massimala (pixel)',
	'label_max_size' => 'Pès massimoum (ko)',
	'label_max_width' => 'Larguessa massimala (pixel)',
	'label_nb_objets_tranches' => 'Noumbre d’ouget en li lista',
	'label_no_autobr' => 'Desativà la pilhada en conte dei alìnea (retour-ligna simple) en lou tèst',
	'label_no_set_html_base' => 'Mìnga d’ajountamen automàticou de <tt>&lt;base href="..."&gt;</tt>',
	'label_options_ecrire_perfo' => 'Proudessa',
	'label_options_ecrire_secu' => 'Segurtà',
	'label_options_skel' => 'Carcul dei pàgina',
	'label_options_typo' => 'Tratamen dei tèstou',
	'label_supprimer_numero' => 'Suprimà automaticamen lu nùmero dei titre',
	'label_toujours_paragrapher' => 'Encapsulà toui lu paràgrafou en un <tt>&lt;p&gt;</tt> (meme lu tèstou coustituit basta d’un paràgrafou)',
	'legend_cache_controle' => 'Countrole de l’amagadou',
	'legend_espace_prive' => 'Espaci privat',
	'legend_image_documents' => 'Image e doucumen',
	'legend_site_public' => 'Sit pùblicou',

	// M
	'message_ok' => 'Lu vouòstre reglage soun estat pihat en conte e registrat en lou fichié <tt>@file@</tt>. Ahura, soun aplicat.',

	// T
	'texte_boite_info' => 'Esta pàgina vi permete de counfigurà facilamen lu reglage escoundut de SPIP.

Se fourçàs d’unu reglage en lou vouòstre fichié <tt>config/mes_options.php</tt>, aquestou fourmulari serà sensa efet soubre aquestu.

Coura serès acabat embé la counfiguracioun dóu vouòstre sit, pourès, se lou voulès, coupià-coulà lou countengut dóu fichié <tt>tmp/ck_options.php</tt> en <tt>config/mes_options.php</tt> denant de desinstalà aquestou plugin que vi farà pu da besoun.',
	'titre_page_couteau' => 'Coutèu KISS',
];
