<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/couteau-kiss.git

return [

	// C
	'ck_description' => 'Simplicité, efficacité, légèreté.
_ Un couteau qui tient vraiment dans la poche en
une unique page de configuration pour les réglages cachés de SPIP.',
	'ck_nom' => 'Couteau KISS',
	'ck_slogan' => 'Simplifier certains réglages SPIP',
];
