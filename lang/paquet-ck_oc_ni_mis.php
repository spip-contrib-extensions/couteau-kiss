<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-ck?lang_cible=oc_ni_mis
// ** ne pas modifier le fichier **

return [

	// C
	'ck_description' => 'Simplicità, eficacha, laugieretà.
_ Un coutèu que ten dau bouòn en pocha, basta en una pàgina de counfiguracioun per lu reglage escoundut de SPIP.',
	'ck_nom' => 'Coutèu KISS',
	'ck_slogan' => 'Simplificà d’unu reglage SPIP',
];
